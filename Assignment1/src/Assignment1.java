import javax.swing.JOptionPane;

public class Assignment1 
{
	public static void main(String[] args) {
		
		String nameString = JOptionPane.showInputDialog("Hello out there.\n" + "What is your name?");
		
		JOptionPane.showMessageDialog(null, "Hello, " + nameString);
				
		String n1String = JOptionPane.showInputDialog("I will be adding three numbers for you.\n" + "Please enter your first number:");
		int n1Count = Integer.parseInt(n1String);
		
		String n2String = JOptionPane.showInputDialog("Please enter your second number:");
		int n2Count = Integer.parseInt(n2String);
		
		String n3String = JOptionPane.showInputDialog("Lastly enter your third number:");
		int n3Count = Integer.parseInt(n3String);
		
		int sumCount = n1Count + n2Count + n3Count;
		
		JOptionPane.showMessageDialog(null,  "The sum of the three numbers = " + sumCount);
		
		System.exit(0); 
	}
}
